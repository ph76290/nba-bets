#!/usr/bin/env python3

import argparse

from src.tools.parse_command_line import parse_datasets_builder_command_line
from src.build_datasets.datasets_builder import DatasetsBuilder


if __name__ == "__main__":

    args = parse_datasets_builder_command_line()
    build_players = args[0]
    build_teams = args[1]
    build_games = args[2]
    logger_filename = args[3]
    verbose = args[4]

    