#!/usr/bin/env python3

import argparse

from src.tools.parse_command_line import parse_scraping_command_line
from src.scraping.scraping_players import ScraperPlayers
from src.scraping.scraping_games import ScraperGames
from src.scraping.team_scraping import TeamScraper
from src.tools.tools import get_teams


if __name__ == "__main__":

    args = parse_scraping_command_line()
    scrap_players = args[0]
    scrap_teams = args[1]
    scrap_games = args[2]
    seasons = args[3]["list"]
    rewrite = args[4]
    logger_filename = args[5]
    verbose = args[6]

    if scrap_players:
        player = ScraperPlayers(seasons, rewrite=rewrite, logger_filename=logger_filename, logger_verbose=verbose)
        player.save_all_players_global_stats()

    if scrap_teams:
        # Need to fix the dict parameter: TODO
        t = set(get_teams())
        teams = TeamScraper(seasons, t,rewrite=rewrite, logger_filename=logger_filename, logger_verbose=verbose)
        teams.scrap_teams()

    if scrap_games:
        games = ScraperGames(seasons, rewrite=rewrite, logger_filename=logger_filename, logger_verbose=verbose)
        games.get_results()