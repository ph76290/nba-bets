from src.scraping.scraper import Scraper
from src.logger import Logger
from src.tools.tools import teams_dict
import requests
import os
import re
import pandas as pd
from bs4 import BeautifulSoup

class ScraperGames(Scraper):
    def __init__(self, seasons: list, playoff: bool = False, logger_filename: str = "stdout", logger_verbose: bool = True, base_url: str = 'https://www.basketball-reference.com/', rewrite: bool = True):
        super().__init__(seasons, playoff, logger_filename, logger_verbose, base_url, rewrite)
        self.col_names = ['date_game', 'visitor_team_name', 'visitor_pts', 'home_team_name', 'home_pts', 'overtimes', 'visitor_wins', 'visitor_losses', 'home_wins', 'home_losses',
                           's1_v', 's1_h', 's2_v', 's2_h', 's3_v', 's3_h', 's4_v', 's4_h', 'ot_v', 'ot_h', 'nb_ot', 'pace_v',
                           'eFG%_v', 'TOV%_v', 'ORB%_v', 'FT/FGA_v', 'ORtg_v', 'pace_h', 'eFG%_h', 'TOV%_h', 'ORB%_h', 'FT/FGA_h', 'ORtg_h',
                           'MP_v', 'FG_v', 'FGA_v', 'FG%_v', '3P_v', '3PA_v', '3P%_v', 'FT_v', 'FTA_v', 'FT%_v', 'ORB_v', 'DRB_v', 'TRB_v', 'AST_v', 'STL_v', 'BLK_v', 'TOV_v', 'PF_v', 'PTS_v',
                           'MP_h', 'FG_h', 'FGA_h', 'FG%_h', '3P_h', '3PA_h', '3P%_h', 'FT_h', 'FTA_h', 'FT%_h', 'ORB_h', 'DRB_h', 'TRB_h', 'AST_h', 'STL_h', 'BLK_h', 'TOV_h', 'PF_h', 'PTS_h',
                           'TS%_v', 'eFG%_v', '3PAr_v', 'FTr_v', 'ORB%_v', 'DRB%_v', 'TRB%_v', 'AST%_v', 'STL%_v', 'BLK%_v', 'TOV%_v', 'USG%_v', 'ORtg_v', 'DRtg_v',
                           'TS%_h', 'eFG%_h', '3PAr_h', 'FTr_h', 'ORB%_h', 'DRB%_h', 'TRB%_h', 'AST%_h', 'STL%_h', 'BLK%_h', 'TOV%_h', 'USG%_h', 'ORtg_h', 'DRtg_h']

    def get_last_checkpoint(self, path_to_csv):
        months = ['october', 'november', 'december', 'january', 'february', 'march', 'april', 'may', 'june']
        try:
            df = pd.read_csv(path_to_csv)
            date = df.tail(1).get('date_game').values[0]
            p = re.compile('[A-Z][a-z]+')
            month = p.findall(date)[0].lower()
            self.logger.add('INFO', 'Starting from {} wich is the last checkpoint'.format(month))
            return months.index(month) + 1
        except:
            return 0

    def get_results(self):
        check_checkpoint = False
        for season in self.seasons:
            path_to_csv = os.path.join('resources/seasons', str(season) + '.csv')
            months = ['october', 'november', 'december', 'january', 'february', 'march', 'april', 'may', 'june']
            self.logger.add('INFO', 'Getting results of the season {}'.format(season))
            if not check_checkpoint and not self.rewrite:
                months = months[self.get_last_checkpoint(path_to_csv):]
                check_checkpoint = True
            
            for month in months:
                df = pd.DataFrame(columns=self.col_names)
                url = "leagues/NBA_" + str(season) + "_games-" + month + ".html"
                page = requests.get(self.base_url + url)
                soup = BeautifulSoup(page.text, 'html.parser')
                rows = soup.find_all('td', {'data-stat': 'box_score_text'})
                for row in rows:
                    a = row.find('a')
                    df = df.append(self.get_boxscores(self.base_url + a['href']), ignore_index=True)

                try:
                    self.write_to_csv(df, path_to_csv, 'a')
                    self.logger.add('INFO', 'Checkpoint successfully saved at {}'.format(path_to_csv))
                except:
                    self.logger.add('ERROR', 'Checkpoint saving failed at {}'.format(path_to_csv))
        
    def get_boxscores(self, url):
        soup = self.get_soup_from_url(url)
        boxscores = {}
        p = re.compile('</?div>')
        boxscores['date_game'] = p.split(str(soup.find(class_="scorebox_meta")))[1]
        
        names = soup.find_all(lambda tag: tag.name == 'a' and tag.has_attr('itemprop') and tag['itemprop'] == 'name')
        boxscores['visitor_team_name'] = teams_dict[names[0].text]
        boxscores['home_team_name'] = teams_dict[names[1].text]
        self.logger.add('INFO', '{} - {} vs {}'.format(boxscores['date_game'], boxscores['visitor_team_name'], boxscores['home_team_name']))

        games = soup.find_all('h2')
        visitor_games = list(map(int, re.findall(r'\d+', games[len(games) // 4].text)))[-2:]
        home_games = list(map(int, re.findall(r'\d+', games[3 * len(games) // 4].text)))[-2:]
        boxscores['visitor_wins'] = visitor_games[0]
        boxscores['visitor_losses'] = visitor_games[1]
        boxscores['home_wins'] = home_games[0]
        boxscores['home_losses'] = home_games[1]

        overtimes, is_home = False, False
        visitor_pts, home_pts, final_scores = [], [], []
        nb_ot, visitor_ots, home_ots = 0, 0, 0
        tds = str(soup.find_all('div' , {'id': 'all_line_score'})).split("<td class='center'>")[1:]
        for i in range(len(tds)):
            nb = list(map(int, re.findall(r'\d+', tds[i])))[0]
            if tds[i].startswith('<strong>'):
                final_scores.append(nb)
                is_home = True
            elif is_home:
                home_pts.append(nb)
            else:
                visitor_pts.append(nb)

        if len(visitor_pts) > 4:
            overtimes = True
            nb_ot = len(visitor_pts[4:])
            visitor_ots = sum(visitor_pts[4:])
            home_ots = sum(home_pts[4:])
        boxscores['visitor_pts'] = final_scores[0]
        boxscores['home_pts'] = final_scores[1]
        boxscores['overtimes'] = overtimes
        for i in range(1, 5):
            boxscores['s' + str(i) + '_v'] = visitor_pts[i-1]
            boxscores['s' + str(i) + '_h'] = home_pts[i-1]
        boxscores['ot_v'] = visitor_ots
        boxscores['ot_h'] = home_ots
        boxscores['nb_ot'] = nb_ot

        p = re.compile(r'<td class="right p?m?')
        tds = p.split(str(soup.find_all('div' , {'id': 'all_four_factors'})[0]))
        for i, td in enumerate(tds[1:], start=self.col_names.index('pace_v')):
            boxscores[self.col_names[i]] = list(map(float, re.findall(r'\d*\.\d+', td)))[0]
        
        def tds_to_cols(col_name, tds):
            for i, td in enumerate(tds, start=self.col_names.index(col_name)):
                try:
                    boxscores[self.col_names[i]] = int(td.text)
                except:
                    boxscores[self.col_names[i]] = float('0' + td.text)

        tds = soup.find_all('tfoot')
        tds_to_cols('MP_v', tds[6].find_all('td')[:-1])
        tds_to_cols('TS%_v', tds[7].find_all('td')[1:])
        tds_to_cols('MP_h', tds[-2].find_all('td')[:-1])
        tds_to_cols('TS%_h', tds[-1].find_all('td')[1:])
        return boxscores