import pandas as pd
import re
import numpy as np
from bs4 import BeautifulSoup


def web_element_to_float(text: str):
    '''
        Function used to convert the web element text to numerical values
    '''
    if text.startswith("."):
        text = "0" + text

    if text.endswith("%"):
        text = text[:-1]

    elif text.endswith("(TW)"):
        text = text[:-5]

    if ':' in text:
        text = int(text.split(":")[0]) + 1

    elif not text or text == '-' or text == '< $Minimum':
        return 0.0

    elif text.startswith("$"):
        text = float(text[1:].replace(",", ""))
    
    elif not text.startswith("-") and '-' in text:
        text = int(text[:4]) + 1 if text[-2:] == "00" else int(text[:2] + text[-2:])

    return float(text) if type(text) != int and type(text) != float else text


def get_dataframe_per_table(soup: BeautifulSoup):
    '''
        Helper to get the dataframe of a player per game
    '''

    soup = BeautifulSoup(str(soup).replace("<!--","").replace("-->",""), 'html.parser')
    table = soup.find("tbody")
    rows = table.find_all("tr")

    headers_useless = [
        "", "ranker", "highs", "over_over_2p", "over_over_3p", "header_pct_fga", "header_fg_pct",
        "header_dunks", "header_corner", "header_heaves", "header_pos_estimates", "header_plus_minus",
        "header_turnovers", "header_fouls", "header_drawn", "header_misc2"
    ]

    headers_html = soup.find("thead").find_all("th")
    headers = [col_name["data-stat"] for col_name in headers_html if col_name.has_attr("data-stat") and not col_name["data-stat"] in headers_useless]

    #print(headers)

    df = pd.DataFrame()

    for row in rows:

        # if not row.has_attr("id"):
        #     continue

        # Declare the dictionary
        dico = dict()

        for header in headers:

            elt = row.find("th", {"data-stat": header}) if row.find("th", {"data-stat": header}) != None else row.find("td", {"data-stat": header})
            
            if elt and header in ["team_id", "lg_id", "pos", "player", "team_name", "college_id"]:
                dico[header] = [elt.text]

            elif elt:
                dico[header] = [web_element_to_float(elt.text)]

        # Append the dictionary to the dataframe

        df = pd.DataFrame.from_dict(dico) \
            if df.empty \
            else df.append(pd.DataFrame.from_dict(dico), sort=True, ignore_index=True)  

    return df


def get_career_dataframe_for_players(soup_player: BeautifulSoup, dico_career: dict):
    '''
        Get the career dataframe for each player
    '''

    career_stats = soup_player.find("div", {"class": "stats_pullout"})
    # 1st career stats
    first_career_stats = career_stats.find("div", {"class": "p1"}).find_all("p")
    dico_career["nb_games"] = [web_element_to_float(first_career_stats[1].text)]
    dico_career["avg_pts"] = [web_element_to_float(first_career_stats[3].text)]
    dico_career["avg_rbd"] = [web_element_to_float(first_career_stats[5].text)]
    dico_career["avg_ast"] = [web_element_to_float(first_career_stats[7].text)]
    # 2nd career stats
    second_career_stats = career_stats.find("div", {"class": "p2"}).find_all("p")
    dico_career["avg_fg"] = [web_element_to_float(second_career_stats[1].text)]
    dico_career["avg_fg3"] = [web_element_to_float(second_career_stats[3].text)]
    dico_career["avg_ft"] = [web_element_to_float(second_career_stats[5].text)]
    dico_career["avg_effective_fg"] = [web_element_to_float(second_career_stats[7].text)]
    # 3rd career stats
    third_career_stats = career_stats.find("div", {"class": "p3"}).find_all("p")
    dico_career["per"] = [web_element_to_float(third_career_stats[1].text)]
    dico_career["ws"] = [web_element_to_float(third_career_stats[3].text)]

    df_career_stats = pd.DataFrame.from_dict(dico_career)

    return df_career_stats


def get_article_headlines_player(soup: BeautifulSoup):
    '''
        Function used to scrap the article headlines from a player page
    '''

    # Declare the dataframe
    df_article_headlines = pd.DataFrame()
   
    # Get the soup containing the news
    soup = BeautifulSoup(str(soup).replace("<!--","").replace("-->",""), 'html.parser')
    content = soup.find("ul", {"class": "news_stories"})
    if not content:
        return df_article_headlines

    # Get the articles html
    articles = content.find_all("li")
    
    # Iterate over the articles to append them in the dataframe
    for article in articles:
        dico = dict()
        if not article.find("a"):
            continue
        article_url = article.find("a")["href"]
        date = re.findall(r'[0-9]{4}\/[0-9]{2}\/[0-9]{2}', article_url)
        if len(date) == 0:
            continue
        
        dico["date"] = [date[0]]
        dico["article_url"] = [article_url]
        dico["headline"] = [article.find("a").text]

        # Append the news in the dataframe
        df_article_headlines = pd.DataFrame.from_dict(dico) \
            if df_article_headlines.empty \
            else df_article_headlines.append(pd.DataFrame.from_dict(dico), sort=True, ignore_index=True) 

    return df_article_headlines