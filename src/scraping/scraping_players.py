from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
from datetime import datetime
from src.scraping.scraper import Scraper
from src.scraping.scraping_tools import web_element_to_float, get_dataframe_per_table, get_career_dataframe_for_players, get_article_headlines_player
import pandas as pd
import numpy as np
import os
import string


class ScraperPlayers(Scraper):
    '''
        Class that inherits from the scraper in order to save all the data for each player
    '''

    def __init__(self, seasons: list, playoff: bool = False, logger_filename: str = "stdout", logger_verbose: bool = True, base_url: str = 'https://www.basketball-reference.com/', rewrite: bool = True):
        '''
            Call the super class to init the players scraper
        '''

        super().__init__(seasons, playoff, logger_filename, logger_verbose, base_url, rewrite)
        self.base_dir = "resources/players/"


    def save_all_players_global_stats(self):
        '''
            Method that save all the players stats for the seasons attribute
        '''

        if self.seasons == []:
            return False

        self.logger.add("INFO", "Saving all the statistics for the seasons attribute of all the players...")
        # Get all the letter of the alphabet in lower case
        alphabet = list(string.ascii_lowercase)

        for letter in alphabet:

            # Get the html table to get all the players starting with the current letter
            soup = self.get_soup_from_url(os.path.join(self.base_url, "players", letter))
            # Check if an error was raised and we could not get the soup
            if not soup:
                continue

            # Get the table containing information for all players of he current letter
            table = soup.find('tbody')
            # Check if the page for this letter contains players
            if not table:
                continue
            
            players = table.find_all('tr')

            for player in players:

                if (player.find('th', {"scope":"row"}) != None):

                    # Get the seasons during which the players played
                    year_from = int(player.find("td", {"data-stat": "year_min"}).text)
                    year_to = int(player.find("td", {"data-stat": "year_max"}).text)

                    player_info = player.find("a")
                    player_name = player_info.text
                    # Checking if the player played during the seasons given as parameters
                    self.logger.add("INFO", "Checking if the player {} played from the season {} to the season {}"
                        .format(player_name, self.seasons[0], self.seasons[-1]))
                    
                    if year_from <= self.seasons[-1] and year_to >= self.seasons[0]:
                        
                        # Create a directory for the player if was already created
                        if not os.path.isdir(os.path.join(self.base_dir, player_name.replace(" ", "_"))):
                            os.makedirs(os.path.join(self.base_dir, player_name.replace(" ", "_")))
                        else:
                            # pass if the player already exist
                            self.logger.add("INFO", "The player already exists in the dataset")
                            continue
                        
                        # Get the player url
                        player_url = player_info["href"][1:]
                        dico_career = dict()
                        dico_career["player_name"] = [player_name]
                        dico_career["player_url"] = [player_url]
                        player_full_url = os.path.join(self.base_url, player_url)

                        # Get the soup of the player page
                        soup_player = self.get_soup_from_url(player_full_url)

                        # Check if the file for this player already exists
                        path = os.path.join(self.base_dir, player_name.replace(" ", "_"), player_name.replace(" ", "_") + "_career.csv")
                        
                        if not os.path.exists(path) or self.rewrite:
                        
                            # Get the dataframe related to the player career stats
                            df_career_stats = get_career_dataframe_for_players(soup_player, dico_career)
                            # Save it
                            self.write_to_csv(df_career_stats, path)

                        # Check if the file for this player already exists
                        path = os.path.join(self.base_dir, player_name.replace(" ", "_"), player_name.replace(" ", "_") + "_articles.csv")
                        
                        if not os.path.exists(path) or self.rewrite:
                            df_articles = get_article_headlines_player(soup_player)
                            self.write_to_csv(df_articles, path)

                        # Declare the tables useful to save
                        useful_tables = [
                            "all_per_game",
                            "all_totals",
                            "all_per_minute",
                            "all_per_poss",
                            "all_advanced",
                            "all_shooting",
                            "all_pbp",
                            "all_year-and-career-highs",
                            "all_playoffs_per_game",
                            "all_playoffs_totals",
                            "all_playoffs_per_minute",
                            "all_playoffs_per_poss",
                            "all_playoffs_advances",
                            "all_year-and-career-highs-po",
                            "all_all_star",
                            "all_all_college_stats",
                            "all_all_salaries"
                        ]

                        # Iterate over all the interesting tables
                        for table in useful_tables:

                            # Check if the tables csv are already written or if the tables even exist
                            path = os.path.join(self.base_dir, player_name.replace(" ", "_"), player_name.replace(" ", "_") + "_" + table + ".csv")
                            table_stats = soup_player.find("div", {"id": table})

                            if (not os.path.exists(path) or self.rewrite) and table_stats:
                                df = get_dataframe_per_table(table_stats)
                                self.write_to_csv(df, path)

        return True