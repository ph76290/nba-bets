import pandas as pd
import requests
from bs4 import BeautifulSoup
from src.logger import Logger

class Scraper:
    '''
        Class used to Scrap the basketball-reference website
        It is used in the scraping_schedule.py / scraping_team.py / scraping_players.py
        with the inheritance concept
    '''

    def __init__(self,  seasons: list, playoff: bool = False, logger_filename: str = "stdout", logger_verbose: bool = True, base_url: str = 'https://www.basketball-reference.com/', rewrite: bool = True):
        '''
            Constructor
            It takes a list named seasons used to know the different years to scrap
            base_url is the url we will use to get our data from
        '''
        self.logger = Logger(logger_filename, logger_verbose)
        if not seasons:
            self.logger.add("ERROR", "The argument seasons is empty")
        self.seasons = seasons
        self.playoff = playoff
        self.base_url = base_url
        self.rewrite = rewrite
        self.logger.add("INFO", "Scraper initialized")

    def write_to_csv(self, df: pd.DataFrame, path: str, mode: str = 'w'):
        '''
            Function used to write a dataframe in a csv
            Return True if it worked
        '''
        with open(path, mode) as f:
            self.logger.add("INFO", "Write a dataframe to the file {} with the mode {}"
                .format(path, mode))
            df.to_csv(f, header=f.tell()==0, index=False)

    def get_soup_from_url(self, url: str):
        '''
            Get the soup from a given url handling the possible exceptions
        '''

        soup = None

        try:
            page = requests.get(url)
            soup = BeautifulSoup(page.text, "html.parser")

        except requests.exceptions.Timeout:
            # Maybe set up for a retry, or continue in a retry loop
            self.logger.add("ERROR", "Connection timed out while getting the soup of the url: {}"
                .format(url))
        except requests.exceptions.TooManyRedirects:
            # Tell the user their URL was bad and try a different one
            self.logger.add("ERROR", "Too many redirections while getting the soup of the url: {}"
                .format(url))
        except requests.exceptions.ConnectionError:
            # Connection error
            self.logger.add("ERROR", "Connection error raised while getting the soup of the url: {}"
                .format(url))
        except requests.exceptions.HTTPError:
            # Http error
            self.logger.add("ERROR", "Error HTTP while getting the soup of the url: {}"
                .format(url))

        return soup