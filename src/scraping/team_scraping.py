from bs4 import BeautifulSoup
from bs4 import Comment
import requests
import re
from src.scraping.scraper import Scraper
from src.scraping.scraping_tools import get_dataframe_per_table
import os

class TeamScraper(Scraper):

    def __init__(self, seasons: list, teams: dict, playoff: bool = False, logger_filename: str = "stdout", logger_verbose: bool = True, base_url: str = 'https://www.basketball-reference.com/teams/', rewrite: bool = True):
        super(TeamScraper, self).__init__(seasons, playoff, logger_filename, logger_verbose, base_url, rewrite)
        if (not teams):
            self.logger.add("ERROR", "The argument teams is empty")
        else:
            self.teams = teams
        
        self.teams_file = os.path.join("resources", "team")
        self.teams_file_col = ["G", "MP", "FG", "FGA" "FG%", "3P", "3PA", "3P%", "2P", "2PA", "2P%", "FT", "FTA", "FT%", "ORB", "DRB", "TRB", "AST", "STL", "BLK", "TOV", "PF", "PTS"]
    
    def scrap_teams(self):
        
        for s in self.seasons:
        
            self.logger.add("INFO", "scraping season " + str(s) + "...")

            season_dir = os.path.join(self.teams_file, str(s))
            os.makedirs(season_dir, exist_ok=True)

            for t in self.teams:

                current_url = self.base_url + t + "/" + str(s) + ".html"

                team_dir = os.path.join(season_dir, t)
                
                os.makedirs(team_dir, exist_ok=True)

                soup = self.get_soup_from_url(current_url)

                self.logger.add("INFO", "scraping " + current_url + "...")

                for table in ["all_per_game"]:
                    found = soup.find("div", { "id":table})
                    df = get_dataframe_per_table(found)

                    self.write_to_csv(df, os.path.join(team_dir, table + ".csv"), mode="w+")
                        

                with open(os.path.join(team_dir, "team_vs_opp.csv"),'w') as f:
                    f.write(",".join(self.teams_file_col) + "\n")

                    
                    page = requests.get(current_url)
                    soup1 = BeautifulSoup(page.text, 'html.parser')
                    elt = soup1.find_all(lambda x: x.name=='div' and x.has_attr('id') and x['id']=="all_team_and_opponent")
                    rows = re.findall(r'<tr >.*</tr>', str(elt))
                    
                    self.logger.add("INFO", "Write a dataframe to the file {}".format(os.path.join(team_dir, "team_vs_opp.csv")))

                    for r in rows:
                        soup_row = BeautifulSoup(r, 'html.parser')
                        cells = soup_row.findChildren("td")
                        stats = []
                        for cell in cells:
                            cell_content = cell.getText()
                            
                            clean_content = re.sub( '\s+', ' ', cell_content).strip()
                            stats.append(clean_content)

                        row_str = ",".join(stats)
                        f.write(row_str + "\n")



                """team_dir = os.path.join(season_dir, t)
                os.makedirs(team_dir, exist_ok=True)

                with open(os.path.join(team_dir, "team_vs_opp.csv"),'w') as f:


                    f.write(",".join(self.teams_file_col) + "\n")

                    

                    self.logger.add("INFO", "scraping " + current_url + "...")

                    page = requests.get(current_url)
                    soup1 = BeautifulSoup(page.text, 'html.parser')
                    elt = soup1.find_all(lambda x: x.name=='div' and x.has_attr('id') and x['id']=="all_team_and_opponent")
                    rows = re.findall(r'<tr >.*</tr>', str(elt))
                    
                    for r in rows:
                        soup_row = BeautifulSoup(r, 'html.parser')
                        cells = soup_row.findChildren("td")
                        stats = []
                        for cell in cells:
                            cell_content = cell.getText()
                            clean_content = re.sub( '\s+', ' ', cell_content).strip()
                            stats.append(clean_content)

                        row_str = ",".join(stats)

                        f.write(row_str + "\n")"""

                
        

    