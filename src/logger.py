import sys
import os

from datetime import datetime
from time import gmtime, strftime


# Class used to store logs in a file stream
class Logger():

    def __init__(self, filename: str, verbose: bool):


        # Check if the filename is not empty
        if filename == "":

            # Use a filename with the date as a name
            filename = datetime.now().strftime("log_%d-%m-%Y_%H-%M-%S.txt")

        # Set the destination in the folder logs
        if not filename.startswith("logs"):
            filename = os.path.join("logs", filename)

        # Open the file to work with file director
        self.file = sys.stdout if filename == "logs/stdout" else open(filename, "w+")
        
        self.verbose = verbose
        self.filename = filename

        self.add("INFO", "A new logger has been instantiated !")
        self.add("INFO", "The logger will display logs in {}"
            .format(self.filename))


    def add(self, type: str, message: str):

        # Add a message from a bot name to the stream with a certain type (INFO, ERROR)
        if self.verbose or type == "ERROR":
            self.file.write("[LOG][{}][{}]: {}\n".format(strftime("%Y-%m-%d %H:%M:%S", gmtime()), type, message))

    
    def add_return_line(self, message: str):

        # Add a message at the line
        if self.verbose:
            self.file.write(message + "\n")


    def add_model_history(self, history):

        # Pretty print the model history
        self.add("INFO", "Printing the model's metric values over the epochs")

        if self.verbose:
            
            nb_epochs = len(history["loss"])
            keys = history.keys()

            # Iterate over the epochs
            for epoch in range(nb_epochs):

                # Build each line with the appropriate values
                message = "\t\tEpoch {}/{}   =>   ".format(epoch + 1, nb_epochs)
                for i, metric in enumerate(keys):

                    message += metric + ": {:05f}".format(round(history[metric][epoch], 5))

                    if i != len(keys) - 1:
                        message += " - "

                # Add the info line
                self.add_return_line(message)


    def kill(self):

        # This method close the stream
        self.add("INFO", "Last line of the logs, killing the logger...")

        # Close the fd only if it is not stdout
        if self.file is not sys.stdout:
            self.file.close()
