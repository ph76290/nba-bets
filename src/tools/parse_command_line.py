import argparse
import json


def command_line_to_boolean(s: str):
    '''
        Function used to parse boolean values in the command line
    '''

    # if s means a True value return True
    if s.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    # else return false
    else:
        return False


def parse_scraping_command_line():
    '''
        Function returning the command line for the scraping script
    '''

    # Define a new parser
    parser = argparse.ArgumentParser(description='Process some arguments for the scraping script')

    # Add the arguments
    parser.add_argument('--scrap_players', type=command_line_to_boolean, default=True,
        help='A boolean value indicating if we want to scrap the players profiles [default=True]')
    parser.add_argument('--scrap_teams', type=command_line_to_boolean, default=True,
        help='A boolean value indicating if we want to scrap the teams information [default=True]')
    parser.add_argument('--scrap_games', type=command_line_to_boolean, default=True,
        help='A boolean value indicating if we want to scrap the games information [default=True]')
    parser.add_argument('--seasons', type=json.loads, default='{"list": []}',
        help='A list specifying the lower and upper bounds of the seasons we want to scrap [default={"list": []}]')
    parser.add_argument('--rewrite', type=command_line_to_boolean, default=False,
        help='A boolean value indicating if we want to scrap the games information [default=False]')
    parser.add_argument('--logger_filename', type=str, default="",
        help='A filename to write the logs or "stdout" to display the logs on the standard output [default=logs/log_day-month-year_hour-minute-seconds.txt]')
    parser.add_argument('--verbose', type=command_line_to_boolean, default=True,
        help='A boolean value to activate the "INFO" logs [default=True]')

    # Get the args from the parser
    args = parser.parse_args()

    return (args.scrap_players, args.scrap_teams, args.scrap_games, args.seasons, args.rewrite, args.logger_filename, args.verbose)


def parse_datasets_builder_command_line():
    '''
        Function returning the command line for the scraping script
    '''

    # Define a new parser
    parser = argparse.ArgumentParser(description='Process some arguments for the datasets builder script')

    # Add the arguments
    parser.add_argument('--build_players', type=command_line_to_boolean, default=True,
        help='A boolean value indicating if we want to scrap the players profiles [default=True]')
    parser.add_argument('--build_teams', type=command_line_to_boolean, default=True,
        help='A boolean value indicating if we want to scrap the teams information [default=True]')
    parser.add_argument('--build_games', type=command_line_to_boolean, default=True,
        help='A boolean value indicating if we want to scrap the games information [default=True]')
    parser.add_argument('--logger_filename', type=str, default="",
        help='A filename to write the logs or "stdout" to display the logs on the standard output [default=logs/log_day-month-year_hour-minute-seconds.txt]')
    parser.add_argument('--verbose', type=command_line_to_boolean, default=True,
        help='A boolean value to activate the "INFO" logs [default=True]')

    # Get the args from the parser
    args = parser.parse_args()

    return (args.scrap_players, args.scrap_teams, args.scrap_games, args.logger_filename, args.verbose)