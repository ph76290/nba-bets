import pandas as pd

from src.logger import Logger


class DatasetsBuilder():
    '''
        This class will be use to buil datasets
    '''

    def __init__(self, logger: Logger):
        '''
            Constructor
        '''
        
        self.logger = logger


    def write_dataset(self, df: pd.DataFrame, path: str):
        '''
            Writing a new datasets with the specified path
        '''

        self.logger.add("INFO", "Writing a new datasets {}"
            .format(path))
        df.to_csv(path, index=False)